module Main where 

import Control.Arrow (first, second)
import Control.Monad (forM_)
import Control.Monad.State 
import Data.List (insert, intersect)

type Address = String 
data ParseState = ParseState
    { csAbba :: Bool
    , csAbbaInHyper :: Bool 
    , csInHyper :: Bool
    , csLast4 :: String 
    , csABAs :: [String]
    , csBABs :: [String]
    } deriving (Show)

parseFile :: String -> [Address]
parseFile = lines

parseAddress :: Address -> ParseState
parseAddress addr = execState (checkAddress addr) initialState
    where initialState = ParseState 
              { csAbba = False
              , csAbbaInHyper = False
              , csInHyper = False
              , csLast4 = ""
              , csABAs = []
              , csBABs = [] } 

supportsTLS :: Address -> Bool
supportsTLS addr = valid (parseAddress addr)
    where valid s = csAbba s && not (csAbbaInHyper s)

supportsSSL :: Address -> Bool
supportsSSL addr = valid (parseAddress addr)
    where valid s = not . null $ intersect (csABAs s) (map invert (csBABs s))
          invert [a,b,_] = [b,a,b]
          invert _ = error "BAB/ABA should have 3 characters"
    
checkAddress :: Address -> State ParseState ()
checkAddress addr = mapM_ check addr 
    where check :: Char -> State ParseState ()
          check '[' = modify (\s -> s { csInHyper = True, csLast4 = "" })
          check ']' = modify (\s -> s { csInHyper = False, csLast4 = "" })
          check c = do
              s <- get
              
              let last4' = c : take 3 (csLast4 s)
              put $ s { 
                csAbba = csAbba s || (not (csInHyper s) && isAbba last4'), 
                csAbbaInHyper = csAbbaInHyper s || (csInHyper s && isAbba last4'),
                csLast4 = last4'
              }

              let last3 = take 3 last4'
              when (isAba last3) $ 
                if (csInHyper s)
                    then modify (\s -> s { csBABs = insert last3 (csBABs s) })
                    else modify (\s -> s { csABAs = insert last3 (csABAs s) })

          isAba [a,b,c] = a == c && a /= b 
          isAba _ = False

          isAbba [a, b, c, d] = a == d && b == c && a /= b 
          isAbba _ = False

main :: IO ()
main = do
    addresses <- parseFile <$> readFile "P7.txt"

    putStr "Number of addresses supporting TLS: "
    print $ length (filter supportsTLS addresses)

    putStr "Number of addresses supporting SSL: "
    print $ length (filter supportsSSL addresses)