module Main where 

import Data.Char (isDigit, toLower)
import Data.List (dropWhileEnd, group, isInfixOf, sort, sortOn)
import Data.Ord (Down(..))

data Room = Room { roomName :: String, roomSectorID :: Int, roomChecksum :: String }
    deriving (Show)

parseRoom :: String -> Room 
parseRoom str = Room
    { roomName = dropWhileEnd (== '-') . takeWhile (not . isDigit) $ str
    , roomSectorID = read (filter isDigit str)
    , roomChecksum = take 5 $ drop (length str - 6) str 
    }

checksum :: Room -> String 
checksum r = take 5 . map head . sortOn (Down . length) . group . sort . filter (/= '-') . roomName $ r

real :: Room -> Bool
real r = checksum r == roomChecksum r

decrypt :: Room -> String 
decrypt r = map shift (roomName r)
    where shift '-' = ' '
          shift c = ([toLower c..'z'] ++ ['a'..'z']) !! offset
          offset = roomSectorID r `mod` 26

main :: IO ()
main = do
    rooms <- map parseRoom . lines <$> readFile "P4.txt"

    putStr "Sum of sector IDs of real rooms: "
    print . sum . map roomSectorID . filter real $ rooms
    
    putStr "Rooms with name including 'north': "
    mapM_ print [ (r, text) | r <- rooms, let text = decrypt r, "north" `isInfixOf` text, real r ]
