import Control.Arrow ((>>>))
import Data.List (tails)

-- input = "..^^."
input = "...^^^^^..^...^...^^^^^^...^.^^^.^.^.^^.^^^.....^.^^^...^^^^^^.....^.^^...^^^^^...^.^^^.^^......^^^^"

next ('^':'^':'.':_) = '^'
next ('.':'^':'^':_) = '^'
next ('^':'.':'.':_) = '^'
next ('.':'.':'^':_) = '^'
next _ = '.'

-- Given a row, return what the next row is
nextLine cs = zipWith const (map next (tails ("." ++ cs ++ "."))) cs

-- Count how many safe squares there are in a row
countSafe = length . filter (== '.')

-- Count how many safe squares there are in N rows, for a given input.
countRows n = 
    iterate nextLine 
    >>> take n
    >>> map countSafe 
    >>> sum

main = do 
    print ((countRows 40) input)
    print ((countRows 400000) input)
    
