module Main where

import Control.Monad.RWS
import Data.Char (toUpper)
import Numeric (showHex)

type Code = String

data Instruction = U | D | L | R | Push deriving (Read)
type Program = [Instruction]
type Pos = (Int, Int)
type Delta = (Int, Int) 

data Keypad = Keypad { validator :: Pos -> Bool, formatter :: Pos -> Code } 

parseLine, parseFile :: String -> [Instruction]
parseLine line = map (\c -> read [c]) line
parseFile text = concatMap (++ [Push]) . map parseLine . lines $ text 
 
runInstruction :: Instruction -> RWS Keypad Code Pos ()
runInstruction L = move (-1, 0)
runInstruction R = move (1, 0)
runInstruction U = move (0, -1)
runInstruction D = move (0, 1)
runInstruction Push = do
    getNum <- asks formatter 
    pos <- get
    tell (getNum pos) 

move :: Delta -> RWS Keypad Code Pos ()
move delta = do 
     pos <- get 
     let pos' = add delta pos
     valid <- asks validator 
     when (valid pos') (put pos')

runProgram :: RWS Keypad Code Pos a -> Keypad -> Code
runProgram program keypad = snd $ evalRWS program keypad start 

add :: Pos -> Pos -> Pos
add (x, y) (x', y') = (x + x', y + y')

start :: Pos
start = (0, 0)

square, diamond :: Keypad
square = Keypad validSquare numSquare
diamond = Keypad validDiamond numDiamond

validSquare, validDiamond :: Pos -> Bool
numSquare, numDiamond :: Pos -> Code

validSquare (x, y) = max (abs x) (abs y) < 2
numSquare (x, y) = show $ 5 + x + (3 * y)

validDiamond (x, y) = abs x + abs y <= 2
numDiamond (0, -2) = "1"
numDiamond (x, -1) = show (3 + x)
numDiamond (x, 0) = show (7 + x)
numDiamond (x, 1) = map toUpper $ showHex (0xB + x) ""
numDiamond (0, 2) = "D" 
numDiamond pos = error $ "Invalid keypad location: " ++ show pos

main :: IO ()
main = do
    program <- mapM_ runInstruction . parseFile <$> readFile "P2.txt"
    putStrLn $ "Code with square keypad: " ++ runProgram program square
    putStrLn $ "Code with diamond keypad: " ++ runProgram program diamond
