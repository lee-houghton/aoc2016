{-# LANGUAGE ParallelListComp #-}

module Main where 

import Control.Applicative ((<|>), pure)
import Control.Monad.ST.Strict
import Data.Array.Unboxed
import Data.Array.ST
import Data.Array.MArray
import Data.Maybe (catMaybes, listToMaybe)
import Text.Regex.PCRE

type Row = Int 
type Col = Int 
type Amount = Int

data Command 
    = Rect Int Int 
    | RotateCol Col Amount
    | RotateRow Row Amount
    deriving (Show)

parseFile :: String -> [Command]
parseFile = catMaybes . map parse . lines 

parse :: String -> Maybe Command
parse line = rect line <|> rotateCol line <|> rotateRow line where
    rect, rotateCol, rotateRow :: String -> Maybe Command
    rect line = match line "^rect (\\d+)x(\\d+)$" $ \m -> Rect (read (m!!0)) (read (m!!1))
    rotateCol line = match line "^rotate column x=(\\d+) by (\\d+)$" $ \m -> RotateCol (read (m!!0)) (read (m!!1))
    rotateRow line = match line "^rotate row y=(\\d+) by (\\d+)$" $ \m -> RotateRow (read (m!!0)) (read (m!!1))

    match :: String -> String -> ([String] -> Command) -> Maybe Command
    match str pat f = f . (\(_,_,_,m) -> m) <$> (str =~~ pat :: Maybe (String, String, String, [String])) 

type Pos = (Int, Int)
type ScreenState = UArray Pos Bool  

screenWidth = 50
screenHeight = 6

runCommands :: [Command] -> ScreenState   
runCommands cmds = runSTUArray $ do
    arr <- newArray ((0,0), (screenWidth, screenHeight)) False
    mapM_ (run arr) cmds
    return arr
    where 
        run :: STUArray s Pos Bool -> Command -> ST s () 
        run arr (Rect x y) = 
            sequence_ [writeArray arr (i,j) True | i <- [0..x-1], j <- [0..y-1]]
        run arr (RotateRow y dx) = do 
            row <- sequence [ readArray arr (x,y) | x <- [0..screenWidth-1] ]
            sequence_ [ writeArray arr ((x + dx) `mod` screenWidth,y) p | x <- [0..screenWidth-1] | p <- row ]
        run arr (RotateCol x dy) = do 
            col <- sequence [ readArray arr (x,y) | y <- [0..screenHeight-1] ]
            sequence_ [ writeArray arr (x, (y + dy) `mod` screenHeight) p | y <- [0..screenHeight-1] | p <- col ]

showScreen :: ScreenState -> String 
showScreen arr = unlines (map showLine [0..screenHeight-1])
    where showLine y = [ showPixel (arr ! (x, y)) | x <- [0..screenWidth-1] ]
          showPixel True = '#'
          showPixel False = ' '

pixelsLit :: ScreenState -> Int 
pixelsLit arr = length . filter id $ elems arr   

main = do
    commands <- parseFile <$> readFile "P8.txt"
    let result = runCommands commands
    putStrLn $ showScreen result  
    putStr "Pixels lit: "
    print (pixelsLit result)