{-# LANGUAGE LambdaCase, TemplateHaskell #-}

module Main where

import Control.Applicative
import Control.Lens
import Control.Monad
import Control.Monad.State
import Data.Char
import Data.Array
import Data.Array.MArray
import Data.Array.IO

type Address = Int 
type Offset = Int
type Value = Int 

data Ref = Reg Address | Imm Value deriving (Show)
data Command 
    = Cpy Ref Address
    | Inc Address
    | Dec Address 
    | Jnz Ref Offset 
    deriving (Show)

parseFile :: String -> [Command]
parseFile = map parseCommand . lines

parseCommand :: String -> Command 
parseCommand ('c':'p':'y':' ':xs) = Cpy (parseRef ref) (parseAddress addr) where (ref, ' ':addr) = break isSpace xs
parseCommand ('i':'n':'c':' ':xs) = Inc (parseAddress xs)
parseCommand ('d':'e':'c':' ':xs) = Dec (parseAddress xs)
parseCommand ('j':'n':'z':' ':xs) = Jnz (parseRef ref) (read val) where (ref, ' ':val) = break isSpace xs
parseCommand cmd = error $ "Invalid command: " ++ cmd

parseAddress :: String -> Address
parseAddress "a" = 0
parseAddress "b" = 1
parseAddress "c" = 2
parseAddress "d" = 3
parseAddress c = error $ "Invalid address: " ++ c 

parseRef :: String -> Ref 
parseRef str | all isAlpha str = Reg (parseAddress str) 
             | otherwise = Imm (read str)

data MachineState 
    = MachineState 
    { _msRegs :: IOUArray Address Value 
    , _msCmds :: Array Offset Command
    , _msPC :: Offset
    }

makeLenses ''MachineState

type Prog a = StateT MachineState IO a

runProgram :: [Value] -> [Command] -> IO [Int] 
runProgram values commands = do 
    regs <- newListArray (0,3) values :: IO (IOUArray Int Int)
    let cmds = (listArray (0, numCommands - 1) commands)
    execStateT loop $ MachineState regs cmds 0
    getElems regs
    where 
        numCommands = length commands 

        readRef :: Ref -> Prog Value
        readRef (Imm val) = return val 
        readRef (Reg src) = readReg src

        readReg :: Address -> Prog Value 
        readReg addr = use msRegs >>= \regs -> liftIO (readArray regs addr)

        writeReg :: Address -> Value -> Prog ()
        writeReg addr value = use msRegs >>= \regs -> liftIO (writeArray regs addr value)

        modifyReg :: Address -> (Value -> Value) -> Prog ()
        modifyReg addr f = writeReg addr . f =<< readReg addr 

        jmp :: Offset -> Prog ()
        jmp offset = msPC %= (+ offset)

        loop :: Prog ()
        loop = do
            MachineState regs cmds pc <- get
            when (pc < numCommands) $ do 
                case cmds ! pc of 
                    Cpy ref addr -> writeReg addr =<< readRef ref
                    Inc addr -> modifyReg addr succ
                    Dec addr -> modifyReg addr pred 
                    Jnz ref offset -> readRef ref >>= \case
                        0 -> return ()
                        _ -> jmp (offset - 1)

                jmp 1
                loop

modifyArray :: (Ix i, MArray a e m) => a i e -> i -> (e -> e) -> m ()
modifyArray arr i f = writeArray arr i =<< fmap f (readArray arr i)

main :: IO ()
main = do 
    cmds <- parseFile <$> readFile "src/P12.txt"

    putStrLn "With all registers initialised to zero:"
    print =<< runProgram [0,0,0,0] cmds

    putStrLn "With register c initialised to one:"
    print =<< runProgram [0,0,1,0] cmds

    putStrLn "Done."