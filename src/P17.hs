{-# LANGUAGE PackageImports, DeriveGeneric, GeneralizedNewtypeDeriving, OverloadedStrings #-}

module Main where

import Control.DeepSeq (force)
import Control.Monad (join)
import Data.Graph.AStar (aStar)
import GHC.Generics (Generic)
import Data.Hashable (Hashable)
import Data.Maybe (listToMaybe)
import Debug.Trace (trace)
import qualified Data.HashSet as HS
import qualified "cryptonite" Crypto.Hash as H
import qualified Data.ByteString.Char8 as BS

data Dir = U | D | L | R deriving (Show, Eq, Ord, Generic)
type Pos = (Int, Int)
type Path = BS.ByteString
data State = State Pos Path deriving (Show, Eq, Ord, Generic)

instance Hashable Dir
instance Hashable State

go :: Dir -> Pos -> Pos
go L (x, y) = (x - 1, y)
go R (x, y) = (x + 1, y)
go U (x, y) = (x, y - 1)
go D (x, y) = (x, y + 1)

moves :: BS.ByteString -> State -> [State]
moves input (State pos path) =
    let passcode = input `BS.append` path
        isOpen c = c `elem` ("bcdef"::String)
        doors = map isOpen (md5 passcode)
    in concatMap next . zip [U, D, L, R] $ doors
    where valid :: Pos -> Bool
          valid (x, y) = x >= 0 && y >= 0 && x < 4 && y < 4
          next :: (Dir, Bool) -> [State]
          next (_, False) = []
          next (dir, _) =
            let pos' = (go dir pos)
            in if valid pos'
                then [State pos' (path `BS.append` BS.pack (show dir))]
                else []

md5 :: BS.ByteString -> String
md5 str = show (H.hash str :: H.Digest H.MD5)

taxicab :: Pos -> Pos -> Int
taxicab (x, y) (x', y') = abs (x' - x) + abs (y' - y)

heuristic :: State -> Int
heuristic (State pos _) = taxicab pos goal

goal :: Pos
goal = (3, 3)

isGoal :: State -> Bool
isGoal (State pos _) = pos == goal

initial :: State
initial = State (0, 0) ""

-- A* search for the shortest path to (3, 3)
solve :: BS.ByteString -> Maybe BS.ByteString
solve input = do
    let dist (State p _) (State p' _) = taxicab p p'
    State pos path <- join $ listToMaybe . reverse <$> aStar (HS.fromList . moves input) dist heuristic isGoal initial
    return path

-- Depth first search
deepest :: BS.ByteString -> Int
deepest input = go 0 initial
    where
        go :: Int -> State -> Int
        go depth s@(State _ path)
            | isGoal s = depth
            | otherwise = case moves input s of
                [] -> -1
                ms ->  maximum (force $ map (go (depth + 1)) ms)

main = do
    print $ solve "ihgpwlah"
    print $ deepest "ihgpwlah"
    putStrLn "---"
    print $ solve "pgflpeqp"
    print $ deepest "pgflpeqp"
