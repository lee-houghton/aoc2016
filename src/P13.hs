module Main where 

import Control.Monad (forM_, guard)
import qualified Data.HashSet as HS
import Data.Char (intToDigit)
import System.Console.ANSI

import Common (aStar, breadthFirstExpand)

type Pos = (Int, Int)
type Input = Int

isWall :: Input -> Pos -> Bool
isWall input (x,y) 
    | x < 0 = True
    | y < 0 = True 
    | otherwise = odd $ countBits 0 val 
    where
        val = (x*x + 3*x + 2*x*y + y + y*y + input)

        countBits n 0 = n 
        countBits n x = let (q, r) = x `quotRem` 2 
                        in countBits (n+r) q

heuristic :: Pos -> Pos -> Int
heuristic (gx, gy) (x, y) = abs (x-gx) + abs (y-gy) 

lmg :: Input -> Pos -> [Pos]
lmg input (x,y) = 
    filter (not . isWall input) 
           [(x+1, y), (x, y+1), (x-1, y), (x, y-1)]

routeLength :: Input -> Pos -> Maybe Int 
routeLength input goalPos = 
    aStar 
        (lmg input) 
        (\_ _ -> 1) 
        (heuristic goalPos) 
        (== goalPos) 
        (1, 1) 

printMap :: Input -> Pos -> Pos -> HS.HashSet Pos -> (Int, Int) -> IO ()
printMap input start goal visited (width, height) = 
    forM_ [0..height-1] printLine 
    where 
        printLine y = putStrLn $ concat [ toChar x y | x <- [0..width-1] ]
    
        toChar x y 
            | isWall input (x,y) = "#"
            | (x,y) == goal = "G"
            | (x,y) == start = "S"
            | (x,y) `HS.member` visited = highlight Yellow "+"
            | otherwise = "."
    
        highlight colour str = setSGRCode [SetColor Foreground Vivid colour] ++ str ++ setSGRCode []

main :: IO ()
main = do
    let input = 1350
    let start = (1,1)
    let goal = (31, 39)
    let viewport = (50, 50)

    let visited = breadthFirstExpand (1,1) (lmg 1350) 50

    putStrLn $ "Map for input " ++ show input ++ ":"
    printMap input start goal visited viewport

    putStrLn $ "Route length to " ++ show goal ++ ":"
    print $ routeLength input goal 
    
    putStrLn $ "Number of reachable locations:"
    print $ HS.size visited 
