module Common
    ( aStar
    , breadthFirstExpand
    ) where

import Data.Hashable
import qualified Data.HashSet as HS
import qualified Data.HashPSQ as Q

-- Data.Graph.AStar is just too slow, or uses too much memory, because it
-- stores the path to the goal, we just need to know the length of the path.
aStar :: (Hashable s, Eq s, Ord s, Show s, Ord c, Num c)
      => (s -> [s]) -- Legal move generator
      -> (s -> s -> c) -- Distance function
      -> (s -> c) -- Heuristic
      -> (s -> Bool) -- Goal checker
      -> s -- Initial state
      -> Maybe c
aStar moves dist heuristic goal start = go (Q.singleton start (heuristic start) 0) HS.empty where
    go open closed = case Q.minView open of
        Nothing -> Nothing
        Just (x, _, cost, open') ->
            if goal x
                then Just cost
                else let closed' = HS.insert x closed
                     in expand x cost open' closed' (HS.toList (HS.fromList (moves x) `HS.difference` closed'))

    expand _ _ open closed [] = go open closed
    expand x cost open closed (y:ys) =
        let cost' = cost + dist x y
            score' = cost' + heuristic y
        in case Q.lookup y open of
            Nothing -> expand x cost (Q.insert y score' cost' open) closed ys -- Not seen before
            Just (p, _)
                | p <= score' -> expand x cost open closed ys -- Existing score is lower
                | otherwise -> expand x cost (Q.insert y score' cost' open) closed ys -- New score is better

breadthFirstExpand :: (Hashable s, Eq s, Ord s) => s -> (s -> [s]) -> Int -> HS.HashSet s
breadthFirstExpand start lmg steps = go steps (HS.singleton start) (HS.singleton start) where
    go 0 _ seen = seen
    go steps edge seen =
        let edge' = HS.fromList (concatMap lmg (HS.toList edge)) `HS.difference` seen
            seen' = seen `HS.union` edge'
        in go (steps-1) edge' seen'
