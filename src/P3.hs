{-# LANGUAGE ViewPatterns #-}

module Main where 

import Data.List (transpose)

type Tri = (Int, Int, Int)
type Line = [Int]

segment :: Int -> [t] -> [[t]]
segment _ [] = []
segment n (splitAt n -> (s, ss)) = s : segment n ss    

readLine :: String -> Line
readLine = map read . words

parseTriangle :: Line -> (Tri)
parseTriangle [x, y, z] = (x, y, z) 
parseTriangle xs = error ("Cannot parse triangle: " ++ show xs)

validTri :: (Num a, Ord a) => (a, a, a) -> Bool
validTri (x, y, z) = x + y > z && x + z > y && y + z > x

main :: IO ()
main = do
    ls <- map readLine . lines <$> readFile "P3.txt"
    let tris1 = map parseTriangle ls 
    let tris2 = concatMap (map parseTriangle . transpose) $ segment 3 ls

    putStr "Part 1: valid triangles: "
    print $ length (filter validTri tris1)

    putStr "Part 2: valid triangles: "
    print $ length (filter validTri tris2)
