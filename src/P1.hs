module Main where 

import Data.List ((\\))
import qualified Data.Set as S; import Data.Set (Set)

type Distance = Int

-- F: Don't turn
-- L/R: Turn left/right
data Turn = F | L | R deriving (Enum, Read, Show, Eq, Ord)

data Step = Step Turn Distance deriving (Show, Read, Eq)
data Heading = N | E | S | W deriving (Enum, Read, Show, Eq, Ord)

type Vec2 a = (a, a) 
newtype Pos = Pos (Vec2 Int) deriving (Show, Eq, Ord)
newtype Dir = Dir (Vec2 Int) deriving (Show, Eq)
data State = State { statePos :: Pos, stateHeading :: Heading } deriving (Show, Eq)

dir N = Dir (0, 1)
dir E = Dir (1, 0)
dir S = Dir (0, -1)
dir W = Dir (-1, 0)

turn F h = h
turn L N = W
turn L heading = pred heading
turn R W = N
turn R heading = succ heading

go (Pos (x, y)) (Dir (dx, dy)) distance = Pos (x + dx * distance, y + dy * distance) 
applyStep (State pos heading) (Step t distance) = 
    let heading' = turn t heading
        pos' = go pos (dir heading') distance
    in State pos' heading'

parseStep (d:ns) = Step (read [d]) (read ns)
parseSteps = map (parseStep . (\\",")) . words

origin = Pos (0, 0)
initialState = State origin N
farness (Pos (x, y)) (Pos (x', y')) = abs (y' - y) + abs (x' - x)

quantize (Step t distance) = Step t 1 : replicate (distance - 1) (Step F 1)

locations = map statePos . scanl applyStep initialState . concatMap quantize

firstRevisit steps = go (locations steps) S.empty
    where go (p:ps) seen | p `S.member` seen = Just p
                         | otherwise = go ps (p `S.insert` seen)
          go [] _ = Nothing 

main = do
    steps <- parseSteps <$> readFile "P1.txt"

    putStrLn "Route taken:"
    mapM_ (print . statePos) $ scanl applyStep initialState steps
    
    putStr "End point (number of steps): "
    print . farness origin . statePos $ foldl applyStep initialState steps

    putStr "First re-visited place: "
    print . firstRevisit $ steps

    putStr "First re-visited place (number of steps): "
    print . fmap (farness origin) . firstRevisit $ steps
