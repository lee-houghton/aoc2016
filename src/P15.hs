data Disk = Disk { numPos :: Int, pos :: Int } deriving (Show, Read, Eq, Ord)

parseDisks :: String -> [Disk]
parseDisks str = map parseDisk (zip [1..] (lines str))
    where
        parseDisk (i, line) =
            let w = words line
            in Disk (read $ w !! 3) (i + (read . init $ w !! 11))

solve :: [Disk] -> [Int]
solve disks = take 1 (filter (\n -> all (ok n) disks) [1..])
    where ok n (Disk np p) = (n+p) `mod` np == 0

main :: IO ()
main = do
    disks <- parseDisks <$> readFile "src/P15.txt"
    print $ solve disks
    print $ solve (disks ++ [Disk 11 (1 + length disks)])

