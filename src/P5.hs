{-# LANGUAGE OverloadedStrings #-}

module Main where 

import Control.Monad (guard)
import qualified Crypto.Hash.MD5 as MD5
import qualified Data.ByteString.Char8 as BS;
import Data.Char (toLower, digitToInt)
import Data.Hex (hex)
import Data.String (fromString)

type DoorID = BS.ByteString
type Password = String
type Hash = BS.ByteString

md5 :: BS.ByteString -> Hash
md5 str = hex (MD5.hash str)

hashes :: DoorID -> [Hash]
hashes doorID = do 
    i <- [(1::Int)..]
    let h = md5 $ doorID `BS.append` fromString (show i)
    guard $ "00000" `BS.isPrefixOf` h
    return h

simplePassword :: BS.ByteString -> String
simplePassword doorID = map (`BS.index` 5) . take 8 $ hashes doorID  

strongerPassword :: BS.ByteString -> String
strongerPassword doorID = head . dropWhile unfinished . scanl update "________" . hashes $ doorID
    where 
        unfinished str = '_' `elem` str
        update str hash = 
            let i = digitToInt (hash `BS.index` 5)
                c = hash `BS.index` 6
            in if i >= 8 || str !! i /= '_' 
                   then str 
                   else take i str ++ (toLower c : drop (i+1) str)

main :: IO ()
main = do 
    let doorID = "ugkcyxxp"
    
    --putStr "Simple password: "
    --print (map toLower $ simplePassword doorID)
        
    putStrLn $ "Stronger password: " ++ strongerPassword doorID
