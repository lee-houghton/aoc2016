module Main where 

import Data.List (group, sort, sortOn, transpose) 
import Data.Ord (Down(..))

decodeBy :: Ord a => (String -> a) -> String -> String
decodeBy sortKey text = map guess . transpose . lines $ text
    where guess chars = head . head . sortOn sortKey . group . sort $ chars

decode1 :: String -> String 
decode1 = decodeBy (Down . length)

decode2 :: String -> String 
decode2 = decodeBy length

main :: IO ()
main = do
    text <- readFile "P6.txt"

    putStr "Decoded word: "
    putStrLn (decode1 text)

    putStr "Decoded word (modified algorithm): "
    putStrLn (decode2 text)