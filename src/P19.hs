{-# LANGUAGE BangPatterns, ViewPatterns #-}

module Main where 

import qualified Data.Sequence as S
import Data.Sequence (Seq, ViewL(..), viewl, (|>))

data Elf = Elf !Int !Int

winner :: Int -> Int
winner numberOfElves = go $ S.fromList (zipWith Elf [1..numberOfElves] (repeat 1)) where
    go :: Seq Elf -> Int
    go (viewl -> EmptyL) = error "No winner"
    go (viewl -> (Elf i _) :< (viewl -> EmptyL)) = i
    go (viewl -> (Elf i n) :< (viewl -> (Elf i' n') :< es)) = go (es |> Elf i (n + n'))

main = do 
    print (winner 5)
    print (winner 3001330)