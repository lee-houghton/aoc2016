{-# LANGUAGE OverloadedStrings, PackageImports #-}

module Main where

import Control.Concurrent (setNumCapabilities)
import Control.Parallel.Strategies
import qualified "cryptonite" Crypto.Hash as H
--import qualified Crypto.Hash.MD5 as MD5
import qualified Data.ByteString.Char8 as BS;
import Data.Char (toLower)
import Data.Hex (hex)
import Data.List (foldl')
import Data.Maybe (listToMaybe)

type Salt = BS.ByteString
type PlainText = BS.ByteString
type Hash = BS.ByteString

hashes :: Int -> Salt -> [Hash]
hashes rounds salt = map f [0..] `using` parBuffer 10 rdeepseq
    where 
        f = hash rounds . (salt `BS.append`) . BS.pack . show

hash :: Int -> PlainText -> Hash
hash 0 str = str
hash n str = hash (n-1) (BS.pack (show (H.hash str :: H.Digest H.MD5)))

keys :: Int -> Salt -> [(Int, Hash)]
keys rounds salt = go 0 (hashes rounds salt) where 
    go n [] = []
    go n (h:hs) = case seq3 h of 
        Nothing -> go (n+1) hs
        Just c -> if any (has5 c) (take 1000 hs) 
            then (n, h) : go (n+1) hs
            else go (n+1) hs

has5 :: Char -> Hash -> Bool 
has5 c str = BS.replicate 5 c `BS.isInfixOf` str

seq3 :: Hash -> Maybe Char 
seq3 str = listToMaybe . map BS.head . filter ((>= 3) . BS.length) $ BS.group str

indexOf64thKey :: Int -> Salt -> Int
indexOf64thKey rounds salt = fst (keys rounds salt !! 63)

main :: IO ()
main = do
    let salt = "ahsbgdzn"
    setNumCapabilities 8

    print . maximum $ ([ hash 1000 (BS.pack (salt ++ show i)) | i <- [0..5000] ] `using` parBuffer 10 rdeepseq)

    --putStrLn $ "With 1 round of hashing:"
    --putStrLn $ "    Index of 64th key for salt 'abc': " ++ show (indexOf64thKey 1 "abc")
    --putStrLn $ "    Index of 64th key for salt '" ++ BS.unpack salt ++ "': " ++ show (indexOf64thKey 1 salt)

    --putStrLn $ "With 1+2016 rounds of hashing:"
    --putStrLn $ "    Index of 64th key for salt 'abc': " ++ show (indexOf64thKey 2017 "abc")
    --putStrLn $ "    Index of 64th key for salt '" ++ BS.unpack salt ++ "': " ++ show (indexOf64thKey 2017 salt)