{-# LANGUAGE ViewPatterns #-}

module Main where 

import Data.Char (isDigit, isSpace)

decompress :: String -> String 
decompress ('(':xs) = 
    let (read -> chars, drop 1 -> xs') = span isDigit xs
        (read -> reps, drop 1 -> xs'') = span isDigit xs'
        (rep, rest) = splitAt chars xs''
    in repeatStr reps rep ++ decompress rest
decompress (x:xs) = x:decompress xs
decompress "" = ""

decompress2 :: String -> String 
decompress2 ('(':xs) = 
    let (read -> chars, drop 1 -> xs') = span isDigit xs
        (read -> reps, drop 1 -> xs'') = span isDigit xs'
        (decompress2 -> rep, rest) = splitAt chars xs''
    in repeatStr reps rep ++ decompress2 rest
decompress2 (x:xs) = x:decompress2 xs
decompress2 "" = ""

decompressedLength2 :: String -> Integer 
decompressedLength2 ('(':xs) = 
    let (read -> chars, drop 1 -> xs') = span isDigit xs
        (read -> reps, drop 1 -> xs'') = span isDigit xs'
        (decompressedLength2 -> repLength, rest) = splitAt chars xs''
    in reps * repLength + decompressedLength2 rest
decompressedLength2 (x:xs) = decompressedLength2 xs + 1
decompressedLength2 "" = 0

repeatStr :: Int -> String -> String
repeatStr 0 str = ""
repeatStr 1 str = str
repeatStr n str = str ++ repeatStr (n-1) str  

main :: IO ()
main = do
    input <- filter (not . isSpace) <$> readFile "P9.txt"

    putStr "Decompressed length: "
    print $ length (decompress input)

    putStr "Decompressed length (v2): "
    print $ decompressedLength2 input