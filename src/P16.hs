import Data.ByteString.Char8 (ByteString)
import qualified Data.ByteString.Char8 as BS

input :: ByteString
input = BS.pack "11100010111110100"

makeData :: Int -> ByteString -> ByteString
makeData diskSize input
    | BS.length input > diskSize = BS.take diskSize input
    | otherwise = makeData diskSize $ BS.append input (BS.cons '0' (BS.reverse (BS.map bitFlip input)))

bitFlip :: Char -> Char
bitFlip '0' = '1'
bitFlip '1' = '0'
bitFlip x = x

pairs :: String -> [(Char, Char)]
pairs (x : x' : xs) = (x, x') : pairs xs
pairs _ = []

parity :: (Char, Char) -> Char
parity (x, y)
    | x == y = '1'
    | otherwise = '0'

checksum :: ByteString -> String
checksum str = go (BS.length str) (BS.unpack str)
    where go len str | odd len = str
                     | otherwise = go (len `div` 2) (map parity (pairs str))

main = do
    putStrLn $ "Input: " ++ BS.unpack input

    let disk1 = makeData 272 input
    putStrLn $ "Disk 1: " ++ BS.unpack disk1
    putStrLn $ "Checksum 1: " ++ checksum disk1

    let disk2 = makeData 35651584 input
    putStrLn $ "Checksum 2: " ++ checksum disk2
