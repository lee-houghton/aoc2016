{-# LANGUAGE TemplateHaskell, GeneralizedNewtypeDeriving, LambdaCase #-}

module Main where 

import Control.Lens (makeLenses, use, (%=))
import Control.Monad (forM_)
import Control.Monad.Loops (whileM_)
import Control.Monad.State (State, execState, MonadState(..), modify, gets)
import Data.Char (isDigit)
import Data.List (find, insert, nub, sort)
import Data.Maybe (fromMaybe)
import Data.Map (Map); import qualified Data.Map as M
import Text.Parsec hiding (State)
import Prelude hiding (iterate)

newtype BotID = BotID Int deriving (Eq, Ord, Show) 
newtype ChipID = ChipID Int deriving (Eq, Ord, Show) 
newtype OutputID = OutputID Int deriving (Eq, Ord, Show)
type Parser a = Parsec String () a

data Destination 
    = Bot BotID 
    | Output OutputID
    deriving (Show, Eq, Ord)

data Command 
    -- BotID gets this chip
    = Supply ChipID BotID 
    -- BotID passes its low chip to the first destination and its high chip to the second destination
    | Pass BotID Destination Destination 
    deriving (Show, Eq, Ord)

command :: Parser Command
command = supply <|> pass where
    supply = Supply <$> (string "value " *> num ChipID) 
                    <*> (string " goes to bot " *> num BotID)
    pass = Pass <$> (string "bot " *> num BotID)
                <*> (string " gives low to " *> dest)
                <*> (string " and high to " *> dest)
    dest = (Output <$ string "output " <*> num OutputID) 
           <|> (Bot <$ string "bot " <*> num BotID)
    
    num :: (Int -> a) -> Parser a
    num con = con . read <$> many digit

parseFile :: String -> Either ParseError [Command] 
parseFile = mapM parse . lines where 
    parse = runParser command () "input"

newtype BotState = BotState { getBotState :: [ChipSource] } deriving (Show)
data ChipSource = Chip ChipID | LowFrom BotID | HighFrom BotID deriving (Show, Eq, Ord)
data FactoryState = FactoryState
    { _fsBots :: Map BotID BotState
    , _fsOutputs :: Map OutputID ChipSource
    }
makeLenses ''FactoryState
initialFactoryState = FactoryState M.empty M.empty

newtype Factory a = Factory { unFactory :: State FactoryState a } 
    deriving (Functor, Applicative, Monad, MonadState FactoryState)

execFactory :: Factory a -> FactoryState
execFactory (Factory f) = execState f initialFactoryState  

-- Executes a list of commands to set the initial chip sources for each bot/output
initFactory :: [Command] -> Factory ()
initFactory commands = 
    forM_ commands $ \case
        Supply chip bot -> give (Chip chip) (Bot bot)
        Pass bot lowDest highDest -> do
            give (LowFrom bot) lowDest 
            give (HighFrom bot) highDest

-- Adds a chip source to a bot/output            
give :: ChipSource -> Destination -> Factory ()
give chip (Output output) = fsOutputs %= M.insert output chip
give chip (Bot bot) = fsBots %= M.alter give' bot where
    give' Nothing = Just (BotState [chip])
    give' (Just (BotState chips)) = Just (BotState (chip:chips))

-- Resolves chip sources until all sources are resolved.
iterate :: Factory ()
iterate = whileM_ (not <$> checkResolved) $ do
    bots <- M.keys <$> use fsBots
    forM_ bots $ \bot -> setBotSources bot =<< mapM resolve =<< getBotSources bot

    outputs <- M.keys <$> use fsOutputs
    forM_ outputs $ \output -> do
        getOutputSource output >>= \case
            Just s -> setOutputSource output =<< resolve s
            Nothing -> return ()
    where
        -- Resolves a chip source by looking up the referenced bot's sources. 
        resolve :: ChipSource -> Factory ChipSource
        resolve s@(LowFrom f) = resolveWith minimum f s
        resolve s@(HighFrom f) = resolveWith maximum f s
        resolve chip = return chip

        resolveWith :: ([ChipSource] -> ChipSource) -> BotID -> ChipSource -> Factory ChipSource
        resolveWith selector srcBot original = select <$> getBotSources srcBot
            where select srcs | all sourceResolved srcs = selector srcs 
                              | otherwise = original

-- Updates a bot's source list.
setBotSources :: BotID -> [ChipSource] -> Factory ()
setBotSources bot sources = fsBots %= M.insert bot (BotState sources)

-- Gets a bot's source list.
getBotSources :: BotID -> Factory [ChipSource]
getBotSources bot = fromMaybe [] . fmap getBotState . M.lookup bot <$> use fsBots

-- Updates an output's source.
setOutputSource :: OutputID -> ChipSource -> Factory ()
setOutputSource output source = fsOutputs %= M.insert output source 

-- Get's an output's source.
getOutputSource :: OutputID -> Factory (Maybe ChipSource)
getOutputSource output = M.lookup output <$> use fsOutputs

-- Returns True if all ChipSources in the factory state are resolved.
checkResolved :: Factory Bool 
checkResolved = (&&) <$> botsResolved <*> outputsResolved where
    outputsResolved = all sourceResolved . M.elems <$> use fsOutputs
    botsResolved = all botResolved . M.elems <$> use fsBots
    botResolved (BotState chips) = all sourceResolved chips

-- Returns True if a ChipSource is an absolute value, False if it refers to another Bot.
sourceResolved :: ChipSource -> Bool
sourceResolved (Chip _) = True 
sourceResolved _ = False

main :: IO ()
main = do
    commands <- parseFile <$> readFile "P10.txt"
    case commands of 
        Left e -> putStr "Parse error: " >> print e
        Right commands -> do
            let (FactoryState bots outputs) = execFactory (initFactory commands >> iterate)

            do
                let desiredBot = [Chip (ChipID 17), Chip (ChipID 61)] 
                case find ((==) desiredBot . sort . getBotState . snd) (M.assocs bots) of 
                    Just (bot, sources) -> putStrLn $ show bot ++ " compares " ++ show sources
                    Nothing -> putStrLn $ "Not found"

            do
                let sources = sequence $ map (\id -> M.lookup (OutputID id) outputs) [0, 1, 2]
                putStr "Outputs 0, 1 and 2 come from: "
                print sources
                case sources of 
                    Just sources -> putStrLn $ "Product: " ++ show (product [ id | Chip (ChipID id) <- sources ])
                    Nothing -> putStrLn "Not all sources defined"
                 
